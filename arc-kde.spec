Name:           arc-kde
Version:        20180614
Release:        2%{dist}
License:        GPL-3.0
Summary:        Arc KDE customization 
Url:            https://github.com/PapirusDevelopmentTeam/
Group:		User Interface/Desktops
Source:		https://github.com/PapirusDevelopmentTeam/%{name}/archive/%{version}.tar.gz
BuildRequires:  automake
BuildArch:      noarch

%description
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.

%package theme
Summary: Plasma and Color themes
Obsoletes: %{name}-style
BuildArch: noarch

%description theme
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc Color Plasma Desktop theme
Arc Dark Plasma Desktop theme

Arc Color color scheme
Arc Dark color scheme

%package kvantum
Summary: Arc Kbantum themes
BuildArch: noarch

%description kvantum
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc ArcDark ArcDarker Kvantum themes

%package decoration
Summary: Arc Arorae themes
BuildArch: noarch

%description decoration
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc Aurorae Window decorations

%package konsole
Summary: Arc Konsole themes
BuildArch: noarch

%description konsole
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc ArcDark Konsole themes

%package konversation
Summary: Arc Konversation themes
BuildArch: noarch

%description konversation
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Papirus Papirus-Dark Konversation themes

%package wallpapers
Summary: Arc Wallpapers for KDE
BuildArch: noarch

%description wallpapers
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc Wallpapers

%package yakuake
Summary: Arc Yakuake themes
BuildArch: noarch

%description yakuake
Arc KDE - This is a port of the popular GTK theme Arc for Plasma 5 desktop with a few additions and extras.
This package contains:

Arc Arc-Dark Yakuake skins

%prep
%setup

%install                                                                                                                                                                                                       make install DESTDIR=%{buildroot} %{?_smp_mflags}
make install DESTDIR=%{buildroot} %{?_smp_mflags}

%files theme
%defattr(-,root,root)
%doc AUTHORS LICENSE README.md
%{_datadir}/color-schemes
%{_datadir}/plasma

%files decoration
%defattr(-,root,root)
%{_datadir}/aurorae

%files konsole
%defattr(-,root,root)
%{_datadir}/konsole

%files konversation
%defattr(-,root,root)
%{_datadir}/konversation

%files kvantum
%defattr(-,root,root)
%{_datadir}/Kvantum

%files wallpapers
%defattr(-,root,root)
%{_datadir}/wallpapers

%files yakuake
%defattr(-,root,root)
%dir %{_datadir}/yakuake
%dir %{_datadir}/yakuake/skins
%{_datadir}/yakuake/skins/arc
%{_datadir}/yakuake/skins/arc-dark

%changelog
* Thu Aug 30 2018 JMiahMan <JMiahMan@unity-linux.org> - 20180614-2
- Rename style to theme

* Tue Aug 28 2018 JMiahMan <JMiahMan@unity-linux.org> - 20180614-1
- Update to latest upstream commits
